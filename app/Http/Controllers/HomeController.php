<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemMaster;
use App\Bobbin;
use App\WeightLog;
use Carbon\Carbon;
use App\NcrDetail;
use App\Filament;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $doffs = array_keys(WeightLog::all()->groupBy('doff_no')->take(-10)->toArray());

        $fromDate = $this->getFromDate();

        $count = WeightLog::where('erp_status', 0)->count();

        return view('home')->with(compact('doffs', 'fromDate', 'count'));
    }

    public function doff($doff)
    {
        $doff = str_replace(' ', '', $doff);
        $status = self::doffCheck($doff);
        if (!$status) {
            return redirect('/')->withErrors(['error' => 'Invalid format Doff No.']);
        }
        if (date('m') <= 3) {
            $financial_start = '01-04-'.(date('Y')-1);
            $financial_end = date('31-03-Y');
        } else {
            $financial_start = date('01-04-Y');
            $financial_end = '31-03-'.(date('Y') + 1);
        }
        $financial_start = date('Y-m-d 00:00:00', strtotime($financial_start));
        $financial_end = date('Y-m-d 23:59:59', strtotime($financial_end));
        $weightLog = WeightLog::where('doff_no', $doff)
                            ->where('doff_date', '>=', $financial_start)
                            ->where('doff_date', '<=', $financial_end)
                            ->get()
                            ->sortBy('spindle');
        // dd($financial_start,$financial_end);
        $material = null;
        $operator = "";
        $fromDate = $this->getFromDate();
        if (count($weightLog) > 0) {
            $doffDate = date('d-m-Y H:i', strtotime($weightLog->first()->doff_date));
            $material = ['id'=>$weightLog->first()->material_id,'material'=>$weightLog->first()->material];
            $operator = $weightLog->first()->op_name;
        } else {
            $doffDate = date('d-m-Y H:i');
        }
        $itemMaster = ItemMaster::all();
        $bobbins = Bobbin::all();
        $reason = NcrDetail::all();
        $last = $weightLog->last();
        $filament = Filament::all();
        $lastDoff = Weightlog::all()->last();
        // dd($lastDoff->tare_weight);

        if (env('MACHINE') == "1") {
            return view('zell-a-weightlog')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } elseif (env('MACHINE') == "2") {
            return view('zell-b-weightlog')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } elseif (env('MACHINE') == "3") {
            return view('kidde-weightlog')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } else {
            dd('Machine Not Selected in ENV..!');
        }
    }

    public function doffCheck($doff)
    {
        $doffSplit = explode('/', $doff);
        $numeric = false;

        if (count($doffSplit) > 1) {
            if (!is_numeric($doffSplit[1])) {
                return false;
            }
        } else {
            for ($i=0; $i < strlen($doff) ; $i++) {
                if (!$numeric) {
                    if (is_numeric($doff[$i])) {
                        $numeric = true;
                    }
                } elseif (!is_numeric($doff[$i])) {
                    return false;
                }
            }
            if (!$numeric) {
                return false;
            }
        }

        return true;
    }
    
    public function createWeightLog(Request $request)
    {
        $processData = $request->all();
        $update = false;
        $rwStatus = $request['rw_status'] ?? 0;
        try {
            if ($processData['weight_status'] == 1) {
                $weightStatus = 1;
            } else {
                $weightStatus = 0;
            }
            $thisDate = Carbon::now()->timezone('Asia/Kolkata')->format('Y-m-d H:i:s');

            $material= ItemMaster::where('id', $processData['material'])->first();

            if (empty($processData['b_id'])) {
                if (!empty($processData['unique_id'])) {
                    $unique_id = $processData['unique_id'];
                } else {
                    $unique_id = mb_strtolower(str_replace(' ', '', $processData['machine'])).(time());
                }

                $weightLog = WeightLog::create([
                        'unique_id' => $unique_id,
                        'material_id' => $processData['material'],
                        'material' => $material->material,
                        'packing_name' => $material->packing_name,
                        'floor_code' => $material->descriptive_name,
                        'machine' => $processData['machine'],
                        'wl_time' => $thisDate,
                        'op_name' => $processData['operator'],
                        'doff_no' => $processData['doff_no'],
                        'spindle' => $processData['spindle_no'],
                        'tare_weight' => $processData['tare_weight'],
                        'material_weight' => $processData['scale_weight'],
                        'total_weight' => round($processData['actual_weight'], 1),
                        'weight_status' => $weightStatus,
                        'rw_status' => $rwStatus,
                        'reason' => $processData['reason'] ?? null,
                        'ncr_status' => $processData['ncr'],
                        'filament_type'=>$processData['filament'],
                        'doff_date' => date('Y-m-d H:i:s', strtotime($processData['doff_time']))
                      ]);
            } else {
                $update = true;
                WeightLog::where('id', $processData['b_id'])->update([
                        'wl_time' => $thisDate,
                        'op_name' => $processData['operator'],
                        'spindle' => $processData['spindle_no'],
                        'tare_weight' => $processData['tare_weight'],
                        'material_weight' => $processData['scale_weight'],
                        'total_weight' => round($processData['actual_weight'], 1),
                        'weight_status' => $weightStatus,
                        'rw_status' => $rwStatus,
                        'ncr_status' => $processData['ncr'],
                        'reason' => $processData['reason'] ?? null,
                        'erp_status' => 0,
                        ]);

                $weightLog = WeightLog::where('id', $processData['b_id'])->first();
            }

            if (env('MACHINE') == "3") {
                if (strpos($weightLog->material, 'DW') !== false) {
                    $this->printQrCode($weightLog);
                }
                $this->printQrCode($weightLog);
            } else {
                $this->printQrCode($weightLog);
                $this->printQrCode($weightLog);
            }
            

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/weight-log");
            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                http_build_query(
                    array(
                      "api-key"=> "UNICO2019wl",
                      "UniqID"=> $weightLog->unique_id,
                      "wl_eq_id"=> $processData['machine'],
                      "wl_date"=> $thisDate,
                      "wl_op"=> $processData['operator'],
                      "wl_part"=> $material->material,
                      "wl_filament"=>$processData['filament'],
                      "wl_floor_code" => $material->descriptive_name,
                      "wl_packing_name" => $material->packing_name,
                      "wl_doff"=> $processData['doff_no'],
                      "wl_spindle"=> $processData['spindle_no'],
                      "wl_tare"=> $processData['tare_weight'],
                      "wl_weight"=> round($processData['actual_weight'], 1),
                      "wl_scale_wt"=> $processData['scale_weight'],
                      "wl_ok"=> $weightStatus,
                      "wl_rw"=> $rwStatus,
                      "wl_min"=> 0,
                      "wl_max"=> 100,
                      "wl_dt_doff"=> $processData['doff_time'],
                      "reason"=> $processData['reason'] ?? null,
                      "update"=>$update,
                      "ncr_status"=>$processData['ncr'],
                      "inspection_reason"=>null,
                      "inspection_status"=>0,
                      "active_status"=>1,
                      "wl_id"=>$weightLog->id
                        )
                    )
             );

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = json_decode(curl_exec($ch));

            curl_close($ch);

            if ($result->status == 1) {
                WeightLog::where('id', $weightLog->id)->update(['erp_status'=>1]);

                return response()->json(['status'=>1,'msg'=>'Successfully Created.']);
            } else {
                WeightLog::where('id', $weightLog->id)->update(['erp_status'=>0]);

                return response()->json(['status'=>0,'msg'=>$result->msg]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>0,'msg'=> $e->getMessage()]);
        }
    }



    public function deleteWeightLog(Request $request)
    {
        $hash = "$2y$12$63QtzcmqetNj4aSehpT3ROrNe7tcvnRa.dgVMgdaVqlplzYAYGe3O";

        if (\Hash::check($request['password'], $hash)) {
            $weightLog = Weightlog::where('id', $request['id'])->first();
            if ($weightLog->erp_status == 1) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/delete-weight-log");
                curl_setopt($ch, CURLOPT_POST, 1);
    
                curl_setopt(
                    $ch,
                    CURLOPT_POSTFIELDS,
                    http_build_query(
                        array(
                            "api-key"=> "UNICO2019wl",
                            'wl_id'=>$weightLog->id,
                            'machine'=>$weightLog->machine,
                            )
                        )
                );
    
                // receive server response ...
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
                $result = json_decode(curl_exec($ch));
    
                curl_close($ch);

                if (empty($result->status)) {
                    return response()->json(['status'=>0,'msg'=>'Unable to connect to ERP.']);
                }

                if ($result->status == true) {
                    $weightLog->delete();
                    return response()->json(['status'=>1,'msg'=>'Successfully deleted.']);
                } else {
                    return response()->json(['status'=>0,'msg'=>$result->msg]);
                }
            } else {
                $weightLog->delete();
                return response()->json(['status'=>1,'msg'=>'Successfully deleted.']);
            }
            
            // Weightlog::where('id', $request['id'])->delete();
        }
        return response()->json(['status'=>0,'msg'=>'Invalid Password..!']);
    }




    public function checkSpindle(Request $request)
    {
        $weightLog = WeightLog::where('doff_no', $request['doff'])->where('spindle', $request['spindle'])->first();
        if (is_null($weightLog)) {
            return response()->json(['status'=>0,'msg'=>'New Spindle.']);
        }
        return response()->json(['status'=>1,'weight_log_id'=>$weightLog->id]);
    }

    public function GetPrintQr(Request $request)
    {
        $weightLog = WeightLog::where('id', $request['id'])->first();

        $this->printQrCode($weightLog);
    }


    public function printQrCode(WeightLog $weightLog)
    {
        $descriptiveName = ItemMaster::where('material', $weightLog->material)->value('descriptive_name');
        $dop = date('d-m-Y', strtotime($weightLog->doff_date));

        $text = "'Seagull:2.1:DP
                INPUT OFF
                VERBOFF
                INPUT ON
                SYSVAR(48) = 0
                ERROR 15,\"FONT NOT FOUND\"
                ERROR 18,\"DISK FULL\"
                ERROR 26,\"PARAMETER TOO LARGE\"
                ERROR 27,\"PARAMETER TOO SMALL\"
                ERROR 37,\"CUTTER DEVICE NOT FOUND\"
                ERROR 1003,\"FIELD OUT OF LABEL\"
                SYSVAR(35)=0
                OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
                PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
                PRINT#1,\"Printing,Media,Clip Default,On\"
                CLOSE #1
                SETUP \"tmp:setup.sys\"
                KILL \"tmp:setup.sys\"
                CLL
                OPTIMIZE \"BATCH\" ON
                PP25,183:AN7
                BARSET \"QRCODE\",1,1,5,2,1
                PB \"$weightLog->unique_id\"
                PP155,188:NASC 8
                FT \"Univers Bold\"
                FONTSIZE 8
                FONTSLANT 0
                PT \"$weightLog->material\"
                PP155,158:PT \"DOFF : $weightLog->doff_no\"
                PP155,129:PT \"DOP : $dop\"
                PP155,98:PT \"SPINDLE NO : $weightLog->spindle\"
                PP21,64:FONTSIZE 7
                PT \"$weightLog->floor_code\"
                LAYOUT RUN \"\"
                PF
                PRINT KEY OFF";



        file_put_contents('output.prn', $text);

        shell_exec('COPY ' .public_path(). '\output.prn /B \\\127.0.0.1\honeywell');
    }

    public function reportSummary()
    {
        $fromDate = $this->getFromDate();

        return view('report-summary')->with(compact('fromDate'));
    }

    public function getSummary(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];

        if ($request['type'] == 'weight') {
            $weightLog = WeightLog::where('wl_time', '>=', date('Y-m-d H:i', strtotime($from)))
                              ->where('wl_time', '<=', date('Y-m-d H:i', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
            $summary = [];
            $count = 0;
            foreach ($weightLog as $key => $value) {
                $summary[$count]['doff'] = $key;
                $summary[$count]['count'] = $value->first()->material;
                $count++;
            }

            return response()->json(['status'=>1,'logs'=>$summary]);
        } else {
            $weightLog = WeightLog::where('doff_date', '>=', date('Y-m-d H:i', strtotime($from)))
                              ->where('doff_date', '<=', date('Y-m-d H:i', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
            $summary = [];
            $count = 0;
            foreach ($weightLog as $key => $value) {
                $summary[$count]['doff'] = $key;
                $summary[$count]['count'] = $value->first()->material;
                $count++;
            }

            return response()->json(['status'=>1,'logs'=>$summary]);
        }
    }

    public function getFromDate()
    {
        $nowDate = date('H:i');
        if (date('H:i', strtotime('07:00')) <= $nowDate && $nowDate <= date('H:i', strtotime('15:00'))) {
            $shift = date('d-m-Y 07:00');
        } elseif (date('H:i', strtotime('15:01')) <= $nowDate && $nowDate <= date('H:i', strtotime('23:00'))) {
            $shift = date('d-m-Y 15:01');
        } else {
            $shift = date('d-m-Y 00:01');
        }

        return $shift;
    }

    public function rewinding($doff)
    {
        $doff = str_replace(' ', '', $doff);
        $status = self::doffCheck($doff);
        if (!$status) {
            return redirect('/')->withErrors(['error' => 'Invalid format Doff No.']);
        }
        if (date('m') <= 3) {
            $financial_start = '01-04-'.(date('Y')-1);
            $financial_end = date('31-03-Y');
        } else {
            $financial_start = date('01-04-Y');
            $financial_end = '31-03-'.(date('Y') + 1);
        }
        $financial_start = date('Y-m-d 00:00:00', strtotime($financial_start));
        $financial_end = date('Y-m-d 23:59:59', strtotime($financial_end));
        $weightLog = WeightLog::where('doff_no', $doff)
                            ->where('doff_date', '>=', $financial_start)
                            ->where('doff_date', '<=', $financial_end)
                            ->get()
                            ->sortBy('spindle');
        $material = null;
        $operator = "";
        $fromDate = $this->getFromDate();
        if (count($weightLog) > 0) {
            $doffDate = date('d-m-Y H:i', strtotime($weightLog->first()->doff_date));
            $material = ['id'=>$weightLog->first()->material_id,'material'=>$weightLog->first()->material];
            $operator = $weightLog->first()->op_name;
        } else {
            $doffDate = null;
        }
        $itemMaster = ItemMaster::all();
        $bobbins = Bobbin::all();
        $reason = NcrDetail::all();
        $last = $weightLog->last();
        $filament = Filament::all();
        $lastDoff = Weightlog::all()->last();
        // return view('rw.rewinding-zell-b')->with(compact('itemMaster','bobbins','weightLog','doff','doffDate','material','operator','reason','last','filament','fromDate','lastDoff'));
        if (env('MACHINE') == "1") {
            return view('rw.rewinding-zell-a')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } elseif (env('MACHINE') == "2") {
            return view('rw.rewinding-zell-b')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } elseif (env('MACHINE') == "3") {
            return view('rw.rewinding-kidde')->with(compact('itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
        } else {
            dd('Machine Not Selected in ENV..!');
        }
    }

    public function getQr(Request $request)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/get-barcode-rewinding");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'unique_id'=>$request['unique_id']
                      )
                  )
           );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);
        if ($result->status == false) {
            return response()->json(['status'=>false,'msg'=>'No data found try other QR Code.']);
        }
        // http_response_code(500);
        // dd($result->data);
        // return $result->data;

        $weightStatus = 1;
        foreach ($result->data as $key => $value) {
            if ($value->weight_status == 0) {
                $weightStatus = 0;
            }
        }

        if ($weightStatus == 1) {
            return response()->json(['status'=>false,'msg'=>'Scanned QR weight log status Ok. Try Other Spindal.']);
        } else {
            return response()->json(['status'=>true,'entry'=>$result->data]);
        }
    }

    public function ncrEntry()
    {
        $bobbins = Bobbin::all();
        $reason = NcrDetail::all();
        $ncrLog = WeightLog::where('weight_status', 0)
                          ->where('rw_status', '1')
                          ->where('ncr_status', '1')
                          ->get();
        return view('ncr.ncr-packing')->with(compact('bobbins', 'reason', 'ncrLog'));
    }

    public function qrNcr(Request $request)
    {
        $data = file('http://erp.shakticords.com/api/get-qr-ncr/'.$request['uniqueId']);
        $data = json_decode($data[0]);

        if ($data->status == false) {
            return response()->json(['status'=>$data->status,'msg'=>$data->msg]);
        }
        return response()->json(['status'=>$data->status,'entry'=>$data->data]);
    }

    public function createNcr(Request $request)
    {
        $oldEntry = json_decode($request['old_entry']);

        $material= ItemMaster::where('material', $oldEntry->material)->first();

        $thisDate = Carbon::now()->timezone('Asia/Kolkata')->format('Y-m-d H:i:s');

        $weightLog = WeightLog::create([
                    'unique_id' => $oldEntry->unique_id,
                    'material_id' => $material->id,
                    'material' => $material->material,
                    'floor_code' => $material->descriptive_name,
                    'packing_name' => $material->packing_name,
                    'machine' => $request['machine'],
                    'wl_time' => $thisDate,
                    'op_name' => $oldEntry->op_name,
                    'doff_no' => $oldEntry->doff_no,
                    'spindle' => $request['spindle_no'],
                    'tare_weight' => $request['tare_weight'],
                    'material_weight' => $request['scale_weight'],
                    'total_weight' => $request['actual_weight'],
                    'weight_status' => 0,
                    'rw_status' => 1,
                    'reason' => $request['reason'],
                    'ncr_status' => 1,
                    'filament_type'=>$oldEntry->filament,
                    'doff_date' => date('Y-m-d H:i:s', strtotime($oldEntry->doff_date))
                  ]);

        $this->printQrCode($weightLog);

        $this->printQrCode($weightLog);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/create-ncr");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'id'=>$oldEntry->id,
                    'spindle'=>$request['spindle_no'],
                    'tare_weight'=>$request['tare_weight'],
                    'reason'=>$request['reason'],
                    'scale_weight'=>$request['scale_weight'],
                    'actual_weight'=>$request['actual_weight'],
                    'wl_time'=>$weightLog->wl_time,
                    "wl_id"=>$weightLog->id
                      )
                  )
           );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        if ($result->status == 1) {
            $update = WeightLog::where('id', $weightLog->id)->update(['erp_status'=>1]);

            return response()->json(['status'=>1,'msg'=>'Successfully Created.']);
        } else {
            return response()->json(['status'=>0,'msg'=>$result->msg]);
        }
    }

    public function bulkUpload()
    {
        $pendingWeight = WeightLog::where('erp_status', 0)->get()->toArray();
        // dd(json_encode($pendingWeight));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/bulk-upload");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'entry'=>json_encode($pendingWeight),
                      )
                  )
           );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        if (is_null($result)) {
            return redirect('/');
        }
        curl_close($ch);
        $result = json_decode($result);
        if ($result->status == true) {
            WeightLog::where('erp_status', 0)->update(['erp_status'=>1]);
            return redirect('/');
        } else {
            return redirect('/');
        }
    }

    public function nextDoff($doff)
    {
        $doffSplit = explode('/', $doff);
        $newDoff = '';
        if (count($doffSplit) > 1) {
            if (is_numeric($doffSplit[1])) {
                $newDoff = $doffSplit[0].'/'.($doffSplit[1]+1);
            }
        } else {
            $text = '';
            $number = '';
            for ($i=0; $i < strlen($doff) ; $i++) {
                if (is_numeric($doff[$i])) {
                    $number = $number.$doff[$i];
                } else {
                    $text = $text.$doff[$i];
                }
            }
            $newDoff = $text.($number+1);
        }
        $url = '/zell-a-weight-log/'.$newDoff;
        return redirect($url);
    }

    public function qrPrint($doff)
    {
        // $doff = str_replace(' ', '', $doff);

        // $status = self::doffCheck($doff);

        // if (!$status) {
        //     return redirect('/')->withErrors(['error' => 'Invalid format Doff No.']);
        // }

        $weightLogs = WeightLog::where('doff_no', $doff)->get()->groupBy('doff_date')->sortBy('spindel');

        if ($weightLogs->count() == 0) {
            return redirect('/')->withErrors(['error' => 'Doff No. Not Found']);
        }

        return view('qr-print')->with(compact('weightLogs'));
    }
}
