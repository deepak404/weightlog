<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WeightLog;
use PDF;

class ReportController extends Controller
{
    public function weightLogReport(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];
        // dd(date('Y-m-d H:i' ,strtotime($to)));
        $weightLog = WeightLog::where('wl_time', '>=', date('Y-m-d H:i:00', strtotime($from)))
                            ->where('wl_time', '<=', date('Y-m-d H:i:59', strtotime($to)))
                            ->get()
                            ->groupBy('doff_no');
        if (env('MACHINE') == "1") {
            return PDF::loadView('reports.weight-log-report', ['weightlog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Report.pdf');
        } elseif (env('MACHINE') == "2") {
            return PDF::loadView('reports.weight-log-report-b', ['weightlog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Report.pdf');
        } else {
            return PDF::loadView('reports.weight-log-report-kidde', ['weightlog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Report.pdf');
        }
    }
    public function weightLogSummary(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];

        if ($request['report_type'] == 'weight') {
            $weightLog = WeightLog::where('wl_time', '>=', date('Y-m-d H:i:00', strtotime($from)))
                              ->where('wl_time', '<=', date('Y-m-d H:i:59', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
        } else {
            $weightLog = WeightLog::where('doff_date', '>=', date('Y-m-d H:i:00', strtotime($from)))
                              ->where('doff_date', '<=', date('Y-m-d H:i:59', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
        }
        if (env('MACHINE') == "2") {
            return PDF::loadView('reports.weight-log-summary-b', ['weightLog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Summary.pdf');
        } else {
            return PDF::loadView('reports.weight-log-summary', ['weightLog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Summary.pdf');
        }
    }
}
