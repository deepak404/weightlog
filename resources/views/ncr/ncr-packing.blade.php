<!DOCTYPE html>
<html>
<head>
    <?php if (env('MACHINE') == '1'): ?>
      <title>Zell A Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '2'): ?>
      <title>Zell B Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '3'): ?>
      <title>Kidde Weight Log</title>
    <?php endif; ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>

        h3{
            margin-top: 0px;
        }
        .material-icons{
            vertical-align: middle;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        select{
            height: 30px;
        }


        label{
            margin-bottom: 10px;
        }


        select, input{
            width: 100%;
            height: 30px !important;
        }

        .form-group{
            margin-bottom: 10px;
        }

        input[type="submit"]{
            margin-top: 30px;
        }

        #recorded-weight-log,#recorded-weight-log-E{
            height: 80vh;
            margin-top: 50px;
            overflow: scroll;
            /* overflow-x:hidden; */
        }
        #main{
            margin-top: 40px;
        }
        input[type="text"][disabled],select[disabled] {
           background-color: #99999947;
        }
        #scale-weight,#actual-weight{
          font-size: 25px;
          font-weight: 800;
          background-color: #91adff;
          color: #141b14;
        }
        #actual-weight{
          font-size: 20px;
        }
        #overwrite>.modal-dialog{
          width: 25%;
        }
        .update-btn{
          width: 49%;
          margin: 20px 0px;
        }
        label[for="material"]{
          display: inline-block;
        }
        #recorded-weight-log,#recorded-weight-log-E{
          padding: 0px !important;
        }
        #recorded-weight-log>table,#recorded-weight-log-E>table{
          font-size: 10px;
          font-weight: bold;
          text-align: center;
        }
        #recorded-weight-log>table>tbody>tr>td,#recorded-weight-log-E>table>tbody>tr>td{
          padding: 1px;
          vertical-align: middle;
        }
        #recorded-weight-log>table>tbody>tr>td:first-child:hover,#recorded-weight-log-E>table>tbody>tr>td:first-child:hover{
          background-color: #91adff;
          cursor:pointer;
        }
        .p-rl-0{
          padding-left: 0px !important;
          padding-right: 5px  !important;
        }
        .weight_btn{
          display: inline-block;
          padding: 10px;
          width: 100%;
          margin-left: 1px;
          text-align: center;
          margin-top: 10px;
          color: white;
          font-weight: bold;
          font-size: 30px;
        }
        label[for='ok']{
          background-color: green;
        }
        label[for='not-ok']{
          background-color: red;
        }
        ul{
          padding: 0px;
          max-height: 200px;
          overflow: auto;
        }
        .reason-li{
          list-style: none;
          padding: 5px;
          font-weight: 600;
        }
        .reason-li:hover{
          background-color: #91adff;
        }
        .selected-reason{
          background-color: #004cb4;
          color: white;
        }
        #weight-log-report>input{
          width: 49% !important;
        }
        .material-icons{
          font-size: 15px !important;
        }
        .selected-td{
          background-color: #004cb4;
          color: white;
        }
        .s-btn{
          margin-top: 0px !important;
        }
    </style>
</head>
<body>
    <div id="loader" class="loader"></div>
<section id="header">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">

                <div class="col-md-6 col-lg-6 col-sm-6">
                    <form action="#" class="col-md-12" name="weight-log-form" id="weight-log-form">
                      <?php if (env('MACHINE') == '1'): ?>
                        <input type="hidden" name="machine" value="Zell A">
                      <?php endif; ?>
                      <?php if (env('MACHINE') == '2'): ?>
                        <input type="hidden" name="machine" value="Zell B">
                      <?php endif; ?>
                      <?php if (env('MACHINE') == '3'): ?>
                        <input type="hidden" name="machine" value="Kidde">
                      <?php endif; ?>
                      <input type="hidden" name="master_id" value="">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="doff-no">Unique ID</label>
                          <input id="unique-input" name="unique_id" type="text" class="text-input" value="">
                        </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="material">Material</label>
                              <input name="material" id="material">
                              <span id="floor_code"></span>
                          </div>
                      </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="spindle-no">Spindle No</label>
                                <!-- <input type="text" class="text-input" name="spindle_no" id="spindle-no" value="" required> -->
                                <select class="text-input" name="spindle_no" id="spindle-no" required>
                                  <?php if (env('MACHINE') == '1'): ?>
                                    <?php for ($i=1; $i <= 30; $i++) { ?>
                                      <option value="{{$i}}">{{$i}}</option>
                                    <?php } ?>
                                  <?php endif; ?>
                                  <?php if (env('MACHINE') == '2'): ?>
                                    <?php $spindale = ['E','W'] ?>
                                    <?php foreach ($spindale as $value): ?>
                                      <?php for ($i=1; $i <= 40; $i++) { ?>
                                        <option value="{{$value.$i}}">{{$value.$i}}</option>
                                      <?php } ?>
                                    <?php endforeach; ?>
                                  <?php endif; ?>
                                  <?php if (env('MACHINE') == '3'): ?>
                                    <?php for ($i=1; $i <= 60; $i++) { ?>
                                      <option value="{{$i}}">{{$i}}</option>
                                    <?php } ?>
                                  <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="filament">Filament</label>
                              <input name="filament" id="filament" type="text">
                          </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tare-weight">Tare Weight</label>
                                <select class="text-input" id='tare-option' style="width:50% !important;">
                                    <option value="">Manual</option>
                                    <?php foreach ($bobbins as $key => $value): ?>
                                        <option value="{{$value->tare_weight}}">{{$value->name}}</option>
                                    <?php endforeach; ?>
                                </select>
                                <input type="text" class="text-input" name="tare_weight" id="tare-weight" value="" style="width:49% !important;" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="hidden" name="ncr" value="1">
                            <label for="ncr-input">NCR Reason</label>
                            <select class="text-input" name="reason">
                              <?php foreach ($reason as $key => $value): ?>
                                <?php if ($value->ncr_account == "NCR (D)"): ?>
                                  <option value="{{$value->defect}}">{{$value->defect}}</option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="scale-weight">Scale Weight</label>
                                <input type="text" class="text-input" name="scale_weight" id="scale-weight" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="actual-weight">Actual Weight</label>
                                <input type="text" class="text-input" name="actual_weight" id="actual-weight" required>
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" name="reason" value="">
                                <input type="hidden" name="ncr" value="1">
                                <label for="not-ok" class="weight_btn">NCR</label>
                                <input type="checkbox" class="weight_status" value="0" id="not-ok">
                            </div>
                        </div> -->

                        <div class="col-md-12 col-lg-12">
                          <input type="submit" class="btn btn-primary s-btn" value="Save">
                          <!-- <button type="button" class="btn btn-primary pull-right" id="get-report">Report</button> -->
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="col-md-12" id="recorded-weight-log-E">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Doff No</th>
                                <th>material</th>
                                <th>Weight</th>
                                <th>Spindle</th>
                                <th>NCR</th>
                                <th>NCR Date</th>
                                <th>Filament</th>
                                <th>Print QR</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($ncrLog as $value): ?>
                                <tr>
                                  <td>{{$value->doff_no}}</td>
                                  <td>{{$value->material}}</td>
                                  <td>{{$value->total_weight}}</td>
                                  <td>{{$value->spindle}}</td>
                                  <td>{{$value->reason}}</td>
                                  <td>{{date('d-m-Y',strtotime($value->wl_time))}}</td>
                                  <td>{{$value->filament_type}}</td>
                                  <td style="text-align:center;"><i class="material-icons print-qr" data-id="{{$value->id}}">print</i></td>
                                </tr>
                              <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<div id="passModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Password to Delete.</h3>
        <form id="delete-form" action="#">
          <input type="hidden" name="id" id="bobbin-id" value="">
          <input type="password" class="text-input" name="password" value="" required placeholder="Password">
          <input class="btn btn-primary" type="submit" name="open" value="Delete">
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>


<script src="{{url('/js/jquery-ui-1.12.1/external/jquery/jquery.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script type="text/javascript" src="{{url('js/loader.js')}}"></script>
<script src="{{url('js/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>


<script type="text/javascript">
$(document).ready(function(){
    $('input[name="unique_id"]').focus();
    var spindleEntry = "";
    $('input[type="text"],input[type="password"]').attr('autocomplete', 'off');

    $('#operator, #doff-val').on('input',function() {
      $(this).val($(this).val().toUpperCase());
    });

    // $(document).on('input','#spindle-no' ,function() {
    //   $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    // });

    $('#scale-weight').on('input propertychange',function(){
      var tare = $('#tare-weight').val();
      $('#actual-weight').val(parseFloat(($(this).val())-parseFloat(tare)).toFixed(2));
    });

    $('#tare-weight').on('input',function() {
      $('#actual-weight').val(parseFloat(($('#scale-weight').val())-parseFloat($(this).val())).toFixed(2));
    });

    $('#tare-option').on('change',function() {
      $('#tare-weight').val($(this).val());
      $('#actual-weight').val(parseFloat(($('#scale-weight').val())-parseFloat($(this).val())).toFixed(2));
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var socket = io('http://127.0.0.1:3000');

    socket.on('weightlog-channel:WeightReceived', function(data){
       $('#scale-weight').val(data);
       $('#actual-weight').val((parseFloat(data)-parseFloat($('#tare-weight').val())).toFixed(2));

    });


    $('#weight-log-form').on('submit',function(e){
        e.preventDefault();
        if (parseFloat($('#scale-weight').val()) > parseFloat($('#tare-weight').val())) {
          if (spindleEntry != "") {
            var formData = $(this).serialize()+"&old_entry="+JSON.stringify(spindleEntry);
            console.log(formData);
            $.ajax({
              type:'POST',
              url:'/create-ncr',
              data:formData,
              success:function(data){
                if (data.status == '1') {
                  location.reload();
                }else if (data.status == '0') {
                  alert('Unable to update ERP - '+data.msg);
                  location.reload();
                }else{
                  alert(data.msg);
                }
              },
              error:function(xhr){
                console.log(xhr.status);
              }
            });
          }else {
            alert('Re-Scan the spindle.');
            location.reload();
          }
        }else{
          alert('Scale Weight should be higher then Tare weight Try new Weight.');
        }
    });



      $('.print-qr').on('click',function() {
        $.ajax({
                type:'POST',
                url:'/print-qr',
                data:'id='+$(this).data('id'),
                success:function(data){
                  // location.reload();
                },
                error:function(xhr){
                    console.log(xhr.status);
                }
            });
      });
      $('.weight_status').on('click',function() {
          $('input[name="weight_status"]').val($(this).val());
          $('#reason').modal('show');
      });

      $('.reason-li').on('click',function() {
        $('input[name="reason"]').val($(this).text());
        $('input[name="ncr"]').val($(this).data('ncr'));
        $('.selected-reason').removeClass('selected-reason');
        $(this).addClass('selected-reason');
      });

      $('#not-ok-save').on('click',function() {
        $('#weight-log-form').submit();
      });

      $('#get-report').on('click',function() {
        // $('#weightLogReport').modal('show');
      });

      // $('#unique-input').on('click',function() {
      //   $('#uniqueId').modal('show');
      // });

      //setup before functions
      var typingTimer;                //timer identifier
      var doneTypingInterval = 1000;  //time in ms, 1 second for example


      $('input[name="unique_id"]').on('input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      function doneTyping () {
        $.ajax({
                type:'post',
                url:'/get-qr-ncr',
                data:'uniqueId='+$('input[name="unique_id"]').val(),
                success:function(data){
                  if (data.status) {
                    $.each(data.entry,function(key,value) {
                      spindleEntry = value;
                      $('#uniqueId').modal('hide');
                      $('#unique-input').val(value.unique_id);
                      $('input[name="master_id"]').val(value.id);
                      $('#material').val(value.material);
                      $('#floor_code').text(value.floor_code);
                      $('#filament').val(value.filament);
                      $('#spindle-no').val(value.spindle);
                    });
                  }else{
                    alert(data.msg);
                    location.reload();
                  }

                },
                error:function(xhr){
                    console.log(xhr.status);
                }
            });
      }

      // $('#uniqueId').on('submit',function(e) {
      //   e.preventDefault();
      //   $.ajax({
      //           type:'post',
      //           url:'/get-qr-ncr',
      //           data:'uniqueId='+$('input[name="unique_id"]').val(),
      //           success:function(data){
      //             if (data.status) {
      //               $.each(data.entry,function(key,value) {
      //                 spindleEntry = value;
      //                 $('#uniqueId').modal('hide');
      //                 $('#unique-input').val(value.unique_id);
      //                 $('input[name="master_id"]').val(value.id);
      //                 $('#material').val(value.material);
      //                 $('#floor_code').text(value.floor_code);
      //                 $('#filament').val(value.filament);
      //               });
      //             }else{
      //               alert(data.msg);
      //               location.reload();
      //             }
      //
      //           },
      //           error:function(xhr){
      //               console.log(xhr.status);
      //           }
      //       });
      //
      //
      // });
});
</script>
</body>
</html>
