<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ZELL B Weight Log Report</title>

    <style>
        html{
            margin: 15px 50px;
            font-family: "Helvetica";

        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
            vertical-align: top;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }

        td{
          vertical-align: top !important;
        }


        #spindle-table{
            margin-top: 2px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            border-top: none !important;
        }
        #east-table tbody>tr>td, #west-table tbody>tr>td{
            padding: 3px;
        }

        #east-table tbody>tr>td:first-child{
            border-left: none !important;
        }

        #east-table, #west-table, #spindle-table{
            border: none !important;
        }

        #east-table th:first-child, #west-table{
            border-left: none !important;
        }

        #east-table th:last-child, #west-table th:last-child{
            border-right: none;
        }

        #detail-table>tbody>tr>td{
            padding: 0px;

        }


        #east-table>tbody>tr>td:last-child, #west-table>tbody>tr>td:last-child{
            border-right: none;
        }

        #total-table>tbody>tr>td{
            padding: 1px;
            border: none;
        }

        #total-table{
            border:none;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
  <?php $pageCount = 1; ?>
<?php foreach ($weightlog as $key => $value): ?>
  <?php if ($pageCount != 1): ?>
    <div class="page-break"></div>
  <?php endif; ?>
  <table style="width: 100%;" id="header-table">
    <tbody>
      <tr>
        <td style="width: 20%; text-align: center;" rowspan="4" colspan="3"><h3>SCPL ERP</h3></td>
        <td rowspan="4" style="width: 50%; text-align: center" colspan="6"><h2>ZELL B WEIGHT LOG REPORT</h2></td>
        <td colspan="4" style="width: 30%;"><p>FOR/PRO/DB 6A</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Rev.No/Date : 01/15.2.19</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Page No : {{$pageCount}}</p></td>

      </tr>
      <tr>
        <td colspan="4"><p>Approved By : G.M</p></td>

      </tr>
    </tbody>
  </table>
  <p style="text-align: center; font-size: 12px; margin-top: 2px; margin-bottom: 2px;">Date From - {{date('d-m-Y H:i',strtotime($from))}} To {{date('d-m-Y H:i',strtotime($to))}}</p>

  <table style="width: 100%;" id="detail-table">
    <tr>
      <td style="width: 33%;">
        <p><span>Machine</span>: {{$value->first()->machine}}</p>
        <p><span>Count</span>: {{$value->first()->material}}</p>
        <p><span>Floor Code</span>: {{$value->first()->floor_code}}</p>
      </td>

      <td>
        <p><span>Filament</span>: {{$value->first()->filament_type}}</p>
        <p><span>Tare</span>: {{$value->first()->tare_weight}}</p>
        <p><span>Doff</span>: {{$value->first()->doff_no}}</p>
      </td>

      <td><p><span>Date & Time</span>: {{date('d-m-Y H:i',strtotime($value->first()->doff_date))}}</p>
        <p><span>Done By</span>: {{$value->first()->op_name}}</p>
      </td>
    </tr>
  </table>

  <table id="spindle-table" style="width: 100%;">

    <tr>
      <td>
        <table style="width: 100%;" id="east-table">
          <tbody>
            <tr>
              <th>Spindle</th>
              <th>Weight</th>
              <th colspan="2">Quality</th>
            </tr>
            <?php
              $eCount = 0;
              $eOkWeight = 0;
              $eNotOkWeight = 0;
             ?>
             <?php for ($i=1; $i <= 31; $i++) { ?>
               <?php if ($value->where('spindle',"E".$i)->count() > 0): ?>
                   <?php foreach ($value->where('spindle',"E".$i) as $log): ?>
                       <tr>
                         <td>{{$log->spindle}}</td>
                         <td>{{$log->total_weight}}</td>
                           <?php if ($log->weight_status == 1): ?>
                             <td>OK</td>
                             <td></td>
                             <?php
                             $eCount += 1;
                             $eOkWeight += $log->total_weight;
                             ?>
                           <?php else: ?>
                             <td></td>
                             <td>{{$log->reason}}</td>
                             <?php
                             $eCount += 1;
                             $eNotOkWeight += $log->total_weight;
                             ?>
                           <?php endif; ?>
                       </tr>
                   <?php endforeach; ?>
                 <?php else: ?>
                   <tr>
                     <td>E{{$i}}</td>
                     <td></td>
                     <td></td>
                     <td></td>
                   </tr>
               <?php endif; ?>
             <?php } ?>
          </tbody>
        </table>
      </td>


      <td>
        <table style="width: 100%; vertical-align:top;" id="west-table">
          <tbody>
            <tr style="vertical-align:top !important; ">
              <th>Spindle</th>
              <th>Weight</th>
              <th colspan="2">Quality</th>
            </tr>
            <?php
              $wCount = 0;
              $wOkWeight = 0;
              $wNotOkWeight = 0;
             ?>
             <?php for ($i=1; $i <= 31; $i++) { ?>
               <?php if ($value->where('spindle',"W".$i)->count() > 0): ?>
                 <?php foreach ($value->where('spindle',"W".$i) as $log): ?>
                     <tr>
                       <td>{{$log->spindle}}</td>
                       <td>{{$log->total_weight}}</td>
                       <?php if ($log->weight_status == 1): ?>
                         <td>OK</td>
                         <td></td>
                         <?php
                         $wCount += 1;
                         $wOkWeight += $log->total_weight;
                         ?>
                       <?php else: ?>
                         <td></td>
                         <td>{{$log->reason}}</td>
                         <?php
                         $wCount += 1;
                         $wNotOkWeight += $log->total_weight;
                         ?>
                       <?php endif; ?>
                     </tr>
                 <?php endforeach; ?>
                 <?php else: ?>
                   <tr>
                     <td>W{{$i}}</td>
                     <td></td>
                     <td></td>
                     <td></td>
                   </tr>
               <?php endif; ?>
             <?php } ?>



          </tbody>
        </table>
      </td>
    </tr>
  </table>


  <table style="width: 50%; margin-top: 10px;" id="total-table">
    <tr>
      <td><strong>Total Entries</strong></td>
      <td>{{$eCount}}</td>
      <td>{{$wCount}}</td>
    </tr>
    <tr>
      <td><strong>Total Ok Weight</strong></td>
      <td>{{$eOkWeight}}</td>
      <td>{{$wOkWeight}}</td>
    </tr>
    <tr>
      <td><strong>Total Not Ok Weight</strong></td>
      <td>{{$eNotOkWeight}}</td>
      <td>{{$wNotOkWeight}}</td>
    </tr>
    <tr>
      <td><strong>Total Weight</strong></td>
      <td>{{$eOkWeight+$eNotOkWeight}}</td>
      <td>{{$wOkWeight+$wNotOkWeight}}</td>
    </tr>
    <tr>
      <td><strong>Total Summary</strong></td>
      <td>{{($eOkWeight+$eNotOkWeight)+($wOkWeight+$wNotOkWeight)}}</td>
      <td></td>
    </tr>
  </table>
  <?php $pageCount++; ?>
<?php endforeach; ?>

</body>
</html>
