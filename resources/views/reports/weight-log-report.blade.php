<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ZELL A Weight Log Report</title>

    <style>
        html{
            margin: 10px;
            font-family: "Helvetica";
        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }


        #spindle-table{
            margin-top: 20px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            /*border-top: none !important;*/
        }

        #spindle-table>tbody>tr>td{
            padding: 2px 5px;
        }
        #spindle-table{
            border: none !important;
        }


        #detail-table>tbody>tr>td{
            padding: 2px;
            height: 1px;
        }


        #total-table>tbody>tr>td{
            padding: 4px;
            border: none;
        }

        #total-table{
            border:none;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<?php $pageCount = 1; foreach ($weightlog as $key => $value): ?>
  <?php $value = $value->sortBy('spindle') ?>
  <?php if ($pageCount > 1): ?>
    <div class="page-break"></div>
  <?php endif; ?>
  <table style="width: 100%;" id="header-table">
    <tbody>
      <tr>
        <td style="width: 20%; text-align: center;" rowspan="4" colspan="3"><h3>SCPL ERP</h3></td>
        <td rowspan="4" style="width: 50%; text-align: center" colspan="6"><h2>ZELL A WEIGHT LOG REPORT</h2></td>
        <td colspan="4" style="width: 30%;"><p>FOR/PRO/DA 5</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Rev.No/Date : 01/15.2.19</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Page No : {{$pageCount}}</p></td>

      </tr>
      <tr>
        <td colspan="4"><p>Approved By : G.M</p></td>

      </tr>
    </tbody>
  </table>

  <p style="text-align: center; font-size: 15px; margin-top: 7px; margin-bottom: 7px;">Date From {{date('d-m-Y H:i',strtotime($from))}} To {{date('d-m-Y H:i',strtotime($to))}}</p>

  <table style="width: 100%;" id="detail-table">
    <tr>
      <td style="width: 33%;">
        <p><span>Machine</span>: {{$value->first()->machine}}</p>
        <p><span>Count</span>: {{$value->first()->material}}</p>
        <p><span>Floor Code</span>: {{$value->first()->floor_code}}</p>
      </td>

      <td>
        <p><span>Filament</span>: {{$value->first()->filament_type}}</p>
        <p><span>Tare</span>: {{$value->first()->tare_weight}}</p>
        <p><span>Doff</span>: {{$value->first()->doff_no}}</p>
      </td>

      <td><p><span>Date & Time</span>: {{date('d-m-Y H:i',strtotime($value->first()->doff_date))}}</p>
        <p><span>Done By</span>: {{$value->first()->op_name}}</p>
      </td>
    </tr>
  </table>
  <table id="spindle-table" style="width: 100%;">

    <tbody>
      <tr>
        <th style="width: 10%;">Spindle</th>
        <th style="width: 10%;">Gross</th>
        <th style="width: 10%;">Tare</th>
        <th style="width: 10%;">Net</th>
        <th colspan="2" style="text-align:center;">Quality</th>
        <th style="width: 30%;"></th>
      </tr>
      <?php
      $totalOk = 0;
      $totalNotOk = 0;
       ?>
       <?php for ($i=1; $i <= 30; $i++) { ?>
         <?php if ($value->where('spindle',$i)->count() > 0): ?>
               <?php foreach ($value->where('spindle',$i) as $log): ?>
                 <tr>
                   <td>{{$log->spindle}}</td>
                   <td>{{$log->material_weight}}</td>
                   <td>{{$log->tare_weight}}</td>
                   <td>{{$log->total_weight}}</td>
                   <?php if ($log->weight_status == 1): ?>
                     <td style="width: 20%;">OK</td>
                     <td style="width: 20%;"></td>
                     <?php $totalOk +=  $log->total_weight?>
                   <?php else: ?>
                     <td style="width: 20%;"></td>
                     <td style="width: 20%;">{{$log->reason}}</td>
                     <?php $totalNotOk +=  $log->total_weight?>
                   <?php endif; ?>
                   <td></td>
                 </tr>
               <?php endforeach; ?>
           <?php else: ?>
             <tr>
               <td>{{$i}}</td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
             </tr>
         <?php endif; ?>
       <?php } ?>
    </tbody>

  </table>


  <table style="width: 50%; margin-top: 10px;" id="total-table">
    <tr>
      <td><strong>Total Entries</strong></td>
      <td>{{$value->count()}}</td>
    </tr>
    <tr>
      <td><strong>Total Ok Weight</strong></td>
      <td>{{$totalOk}}</td>
    </tr>
    <tr>
      <td><strong>Total Not Ok Weight</strong></td>
      <td>{{$totalNotOk}}</td>
    </tr>
    <tr>
      <td><strong>Total Weight</strong></td>
      <td>{{$totalOk+$totalNotOk}}</td>
      <td></td>
    </tr>
  </table>
  <?php $pageCount++; ?>
<?php endforeach; ?>

</body>
</html>
