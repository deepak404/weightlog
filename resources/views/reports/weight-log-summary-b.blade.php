<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (env('MACHINE') == '1'): ?>
      <title>Zell A Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '2'): ?>
      <title>Zell B Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '3'): ?>
      <title>Kidde Weight Log</title>
    <?php endif; ?>

    <style>
        html{
            margin: 10px;
            font-family: "Helvetica";
        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }


        #spindle-table{
            margin-top: 20px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            /*border-top: none !important;*/
        }

        #spindle-table>tbody>tr>td{
            padding: 2px 5px;
        }
        #spindle-table{
            border: none !important;
        }


        #detail-table>tbody>tr>td{
            padding: 2px;
            height: 1px;
        }


        #total-table>tbody>tr>td{
            padding: 4px;
            border: none;
        }
        #total-table{
            border:none;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>

  <table style="width: 100%;" id="header-table">
    <tbody>
      <tr>
        <td style="width: 20%; text-align: center;" rowspan="4" colspan="3"><h3>SCPL ERP</h3></td>
        <td rowspan="4" style="width: 50%; text-align: center" colspan="6"><h2>Zell A Weight Summary Report</h2></td>
        <td colspan="4" style="width: 30%;"><p>FOR/PRO/DB 6A</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Rev.No/Date : 01/15.2.19</p></td>
      </tr>
      <tr>
        <td colspan="4"><p>Page No : 1</p></td>

      </tr>
      <tr>
        <td colspan="4"><p>Approved By : G.M</p></td>

      </tr>
    </tbody>
  </table>

  <p style="text-align: center; font-size: 15px; margin-top: 7px; margin-bottom: 7px;">Date From {{$from}} To {{$to}}</p>

  <table id="spindle-table" style="width: 100%;">

    <thead>
      <tr>
        <th>Date</th>
        <th>Shift</th>
        <th>Count</th>
        <th>Doff</th>
        <th>Entries</th>
        <th>Gross</th>
        <th>Net</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($weightLog as $key => $value): ?>
        <tr>
          <?php
          $shift = 1;
          $doffTime = date('H:i',strtotime($value[0]->doff_date));
          if (date('H:i',strtotime('07:00')) <= $doffTime && $doffTime <= date('H:i',strtotime('15:00'))) {
            $shift = 1;
          }elseif (date('H:i',strtotime('15:01')) <= $doffTime && $doffTime <= date('H:i',strtotime('23:00'))) {
            $shift = 2;
          }else {
            $shift = 3;
          }
          ?>
          <?php if ($shift == 3): ?>
            <td>{{date('d-m-Y',strtotime("-1 days",strtotime(date('d-m-Y',strtotime($value[0]->doff_date)))))}}</td>
            <?php else: ?>
              <td>{{date('d-m-Y',strtotime($value[0]->doff_date))}}</td>
          <?php endif; ?>
          <td>{{$shift}}</td>
          <td>{{$value[0]->material}}</td>
          <td>{{$value[0]->doff_no}}</td>
          <td>{{$value->count()}}</td>
          <td>{{$value->sum('material_weight')}}</td>
          <td>{{$value->sum('total_weight')}}</td>
        </tr>
      <?php endforeach; ?>
    </tbody>

  </table>



</body>
</html>
