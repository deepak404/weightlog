<!DOCTYPE html>
<html lang="en">
<head>
    <?php if (env('MACHINE') == '1'): ?>
        <title>Zell A Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '2'): ?>
        <title>Zell B Weight Log</title>
    <?php endif; ?>
    <?php if (env('MACHINE') == '3'): ?>
        <title>Kidde Weight Log</title>
    <?php endif; ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
</head>
<body>
    <section id="header">
        <header>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="active-menu" href="/">Home</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    
    </section>
    <section id="main">
        <div class="container">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Doff No</th>
                    <th>Doff Date</th>
                    <th>Spindle</th>
                    <th>Material</th>
                    <th>Floor Code</th>
                    <th>Filament</th>
                    <th>Material Weight</th>
                    <th>Actual Weight</th>
                    <th>Print</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($weightLogs as $date => $weightLog)
                        @foreach ($weightLog as $item)
                            <tr>
                                <td>{{$item->doff_no}}</td>
                                <td>{{$item->doff_date}}</td>
                                <td>{{$item->spindle}}</td>
                                <td>{{$item->material}}</td>
                                <td>{{$item->floor_code}}</td>
                                <td>{{$item->filament_type}}</td>
                                <td>{{$item->total_weight}}</td>
                                <td>{{$item->material_weight}}</td>
                                <td style="text-align:center;"><i class="material-icons print-qr" data-id="{{$item->id}}">print</i></td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
              </table>
        </div>
    </section>

    <script src="{{url('/js/jquery-ui-1.12.1/external/jquery/jquery.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{url('js/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.print-qr').on('click',function() {
            $.ajax({
                    type:'POST',
                    url:'/print-qr',
                    data:'id='+$(this).data('id'),
                    success:function(data){
                        console.log(data);
                    },
                    error:function(xhr){
                        console.log(xhr.status);
                    }
                });
          });
        });
    </script>
</body>
</html>