var server = require('http').Server();

var io = require('socket.io')(server);


// This will return a class equivalent
var Redis = require('ioredis');


//Creating object for the defined redis class
var redis = new Redis();

redis.subscribe('weightlog-channel');


redis.on('message',function(channel, message){

    // console.log('Message received');
    //
    console.log(message, channel, 'hello');

    message = JSON.parse(message);

    console.log(message)

    io.emit(channel + ':' + message.event, message.data);
});


server.listen(3000, function(){
    console.log('server running');
});