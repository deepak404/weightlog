import serial
import requests
import re
import time

API_KEY = 'dummy'
API_ENDPOINT = 'http://127.0.0.1:8000/api/get-weight-feed'
ser = serial.Serial();
ser.port = 'COM3'
ser.baudrate=9600
ser.timeout=1
ser.parity=serial.PARITY_ODD
ser.stopbits=serial.STOPBITS_TWO
ser.bytesize=serial.SEVENBITS
ser.open();
print("connected to: " + ser.portstr)
print('Please start scanning with the Scanner...')



while True:
    bytesToRead = ser.inWaiting()
    data = re.findall("\d+\.\d+", ser.read(bytesToRead).decode('utf-8'))
    if data:
        weight = max(data)
        print(weight)
        POST_DATA = {'data':weight,'event':'WeightReceived'}
        response = requests.post(url = API_ENDPOINT, data = POST_DATA)
        print('Response from API : '+response.text)
    time.sleep(1)

