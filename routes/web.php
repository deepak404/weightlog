<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

date_default_timezone_set('Asia/Kolkata');


Route::get('/', 'HomeController@index');

Route::get('/zell-a-weight-log/{doff}', 'HomeController@doff')->where('doff', '(.*)');

Route::get('/rewinding/{doff}', 'HomeController@rewinding')->where('doff', '(.*)');

Route::get('/next-doff/{doff}','HomeController@nextDoff')->where('doff', '(.*)');

Route::get('/qr-print/{doff}','HomeController@qrPrint')->where('doff', '(.*)');

Auth::routes();

Route::post('/zell-a-weight-create', 'HomeController@createWeightLog');

Route::post('/delete-weight-log', 'HomeController@deleteWeightLog');

Route::post('/check-spindle', 'HomeController@checkSpindle');

Route::post('/print-qr', 'HomeController@GetPrintQr');

Route::get('/report-summary', 'HomeController@reportSummary');

Route::post('/get-summary', 'HomeController@getSummary');

Route::post('/getQR', 'HomeController@getQr');

Route::get('/ncr-entry', 'HomeController@ncrEntry');

Route::post('/get-qr-ncr', 'HomeController@qrNcr');

Route::post('/create-ncr', 'HomeController@createNcr');

Route::get('/bulk-upload', 'HomeController@bulkUpload');


// ********************* Reports ******************************

Route::post('/weight-log-report', 'ReportController@weightLogReport');

Route::post('/generate-report', 'ReportController@weightLogSummary');
